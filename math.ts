// funcion con nombre o nombrada
function suma(x: number , y: number):number {
    return x + y
}
// funcion flecha
const sumaFlecha = (x: number , y: number):number => {
    return x + y
}
// console.log(suma(1,2) );

// funcion con parametros opcionales
const funcionOpcionalSuma = (x:number, y?:number):number =>{
    if(!y) return x 
    return x + y
}
// console.log(funcionOpcionalSuma(1));
// console.log(funcionOpcionalSuma(1,2));

// Otra opcion es agregarle un valor por defecto
const funcionOpcionalSuma2 = (x:number, y:number = 0):number =>{
    return x + y
}

const resta = (x:number, y:number = 0):number =>{
    return x - y 
}

const mult = (x:number, y:number = 1):number =>{
    return x * y 
}
console.log('SUMA');
console.log(funcionOpcionalSuma2(1));
console.log(funcionOpcionalSuma2(1,2));
console.log('RESTA');
console.log(resta(3,1));
console.log(resta(3));
console.log('MULTIPLICACION');
console.log(mult(3,2));
console.log(mult(3));
